# data "aws_ami" "fargate_gitlab_mgr_ami" {
#   most_recent = true
#   owners      = ["amazon"]

#   filter {
#     name   = "name"
#     values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
#   }

#   filter {
#     name   = "root-device-type"
#     values = ["ebs"]
#   }

#   filter {
#     name   = "virtualization-type"
#     values = ["hvm"]
#   }
# }

data "template_file" "user-init-fargate-mgr" {
  template = "${file("${path.module}/files/userdata_fargate_mgr.tpl")}"
}

data "aws_iam_policy" "ssm_role_policy" {
  arn = "${var.ssm_role_policy}"
}

data "aws_iam_policy" "ecs_full_access" {
  arn = "${var.ecs_full_access_policy}"
}